<?php
/*
  ./app/vues/posts/index.php
  Variables disponibles :
      -
 */
 ?>

 <h1 class="page-header">
     Material Design for Bootstrap
     <small>made with love</small>
 </h1>

 <?php foreach ($posts as $post): ?>
   <article class="">
     <h2>
         <a href="post/material-design-for-bootstrap"><?php echo $post->getTitre(); ?></a>
     </h2>
     <p class="lead">
       by <a href="#">pascal</a>
     </p>
     <p> Posted on
       <?php echo $post->getDatePublication(); ?>     </p>
     <hr>
        <div><?php echo \Noyau\Fonctions\Tronquer($post->getTexte()); ?></div>
     <a href="post/material-design-for-bootstrap">
       <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
     </a>
     <hr>
   </article>
 <?php endforeach; ?>
