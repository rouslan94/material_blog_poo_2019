<?php
/*
  ./app/vues/templates/defaut.php
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include '../app/vues/templates/partials/_head.php'; ?>
</head>

<body>
  <!-- Navigation -->
  <?php include '../app/vues/templates/partials/_nav.php'; ?>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-8">
        <?php echo $content1; ?>
      </div>

      <!-- Blog Sidebar Widgets Column -->
      <div class="col-md-4">

      </div>
    </div>
    <!-- /.row -->

    <hr>

  </div>
  <!-- /.container -->


  <!-- Footer -->
  <?php include '../app/vues/templates/partials/_footer.php'; ?>



  <!-- SCRIPTS -->
  <?php include '../app/vues/templates/partials/_scripts.php'; ?>


</body>

</html>
