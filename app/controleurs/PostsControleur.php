<?php
/*
  ./app/controleurs/postsControleur
 */

namespace App\Controleurs;
use App\Modeles\Post;

class PostsControleur {

  public function indexAction(\PDO $connexion){
    // Je demande au modele la liste des posts
    $gestionnaire = new \App\Modeles\PostsGestionnaire();
    $posts = $gestionnaire->findAll($connexion);

    // Je charge la vue index dans $content1
    GLOBAL $content1, $title;
    $title = POSTS_INDEX_TITLE;
    ob_start();
      include '../app/vues/posts/index.php';
    $content1 = ob_get_clean();
  }
}
