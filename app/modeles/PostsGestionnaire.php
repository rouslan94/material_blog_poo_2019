<?php
/*
  ./app/modeles/PostsGestionnaire
 */

namespace App\Modeles;

class PostsGestionnaire {

    public function findAll(\PDO $connexion){
      $sql = "SELECT *, posts.id as postID
              FROM posts
              JOIN auteurs ON posts.auteur = auteurs.id
              ORDER BY datePublication DESC
              LIMIT 5;";
      $rs = $connexion->query($sql);
      $posts = $rs->fetchAll(\PDO::FETCH_ASSOC);


      $tableau = [];
      foreach ($posts as $post) {
        $tableau[] = new Post($post);
      }
      return $tableau;
    }

}
