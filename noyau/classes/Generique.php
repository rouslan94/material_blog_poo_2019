<?php

namespace Noyau\Classes;

abstract class Generique {

  public function __construct(array $data = null) {
    if ($data):
      $this->hydrater($data);
    endif;
  }

  public function hydrater(array $data = null){
    if($data):
      foreach ($data as $propriete => $valeur) {
        $methode = 'set' . ucfirst($propriete);
        if(method_exists($this, $methode)):
          $this->$methode($valeur);
        endif;
      }
    endif;
  }

}
